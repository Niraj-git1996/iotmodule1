# Introduction to Industrial IOT(IIOT)

* Industry 4.0 refers to a new phase in the Industrial Revolution that focuses heavily on interconnectivity, automation, machine learning, and real-time data.
* **Industrial Internet of Things (IIoT)** Industry 4.0, also sometimes referred to as IIoT or smart manufacturing, marries physical production and operations with smart digital technology, machine learning, and big data to create a more holistic and better connected ecosystem for companies that focus on manufacturing and supply chain management.
<div align="center">![revoultion](/extras/1.jpg)</div>

## Evolution of Industry from 1.0 to 4.0

Before digging too much deeper into the what, why, and how of Industry 4.0, it’s beneficial to first understand how exactly manufacturing has evolved since the 1800s. 

### The First Industrial Revolution

* The first industrial revolution happened between the late 1700s and early 1800s. During this period of time, manufacturing evolved from focusing on manual labor performed by people.
* The aided by work animals to a more optimized form of labor performed by people through the use of water and steam-powered engines and other types of machine tools.

### The Second Industrial Revolution

* The world entered a second industrial revolution with the introduction of steel and use of electricity in factories.
* The introduction of electricity enabled manufacturers to increase efficiency and helped make factory machinery more mobile.
*  It was during this phase that mass production concepts like the assembly line were introduced as a way to boost productivity.

### The Third Industrial Revolution

* A third industrial revolution slowly began to emerge, as manufacturers began incorporating more electronic—and eventually computer—technology into their factories. 

Take a look at the pyramid given below for a Bird's-eye view

<div align="center">![revoultion](/extras/5.png)</div>

#### Architecture of Industry 3.0:

<div align="center">![revoultion](/extras/6.png)</div>

* **Field Devices** - They are at the bottom of the Hierarchy. These are the sensors or actuators present in the factory, they measuring things or actuate machines (doing some actions).

* **Control Devies** - These are the PCs , PLCs etc. They tell the field devices what to do and control them.

* **Station** - A set of Field devices & Control Devices together is called a station.

* **Work Centres** - A set of stations together is called a work centre (Factories). They use softwares like Scada & Historian.

* **Enterprise** - A set of Work Centres together becomes an Enterprise. They use softwares like ERP & MES.

The softwares used like Scada, Erp etc are used to store data, arrange data given to them by the field devices through the control devices.

#### Industry 3.0 communication protocols

<div align="center">![revoultion](/extras/7.png)</div>

### Industrial 4.0

* Fourth industrial revolution has emerged, known as Industry 4.0. Industry 4.0 takes the emphasis on digital technology from recent decades to a whole new level with the help of interconnectivity through the Internet of Things (IoT)
* It connects physical with digital, and allows for better collaboration and access across departments, partners, vendors, product, and people

#### IOT Decision FrameWork

* The IoT decision framework is much more important as the product or services communicates over networks goes through five different layers of complexity of technology.

<div align="center">![revoultion](https://static.javatpoint.com/tutorial/iot/images/iot-decision-framework.png)</div>

#### Communication protocols

*  **Below Image show the migration of Industry 3.0 communication protocols to IOT protocols**

<div align="center">![revoultion](/extras/10.png)</div>

#### Industry 4.0 Architecture

<div align="center">![revoultion](/extras/9.png)</div>

1. **Sensors/Actuators:** Sensors or Actuators are the devices that are able to emit, accept and process data over the network. These sensors or actuators may be connected either through wired or wireless. This contains GPS, Electrochemical, Gyroscope, RFID, etc
2. **Gateways and Data Acquisition:** As the large numbers of data are produced by this sensors and actuators need the high-speed Gateways and Networks to transfer the data. This network can be of type Local Area Network (LAN such as WiFi, Ethernet, etc.), Wide Area Network (WAN such as GSM, 5G, etc.)

3. **Edge IT:** Edge in the IoT Architecture is the hardware and software gateways that analyze and pre-process the data before transferring it to the cloud. 

4. **Data center/ Cloud:** The Data Center or Cloud comes under the Management Services which process the information through analytics, management of device and security controls.


#### Problems faced by Factory Owners for Industry 4.0 Upgradation and their solution
***
### Problems

1. **Cost:** Factory owners don't want to switch into Industry 4.0, because it is _Expensive_.
2. **Downtime:** Changing Hardware would result in downtime and nobody want to face such loss.
3. **Reliability:** Investing in devices which are unproven and unreliable is a risk.


#### Solutions

* Its pretty solution is : "Getting data from the devices already present in the factory and sending it to cloud."
* So, there is no need to replace the original devices, causing lower risk factor.

* In simple language, "Get data from _Industry 3.0_ and convert it to _Industry 4.0_."
* We have to **Change the _Industry 3.0 Protocols_ to _Industry 4.0 Protocols_.**

* However some Challenges faced in this Conversion are :
  * Expensive Hardware
  * Lack of documentation
  * Propritary PLC Protocols

#### Conversion of Industry 3.0 to Industry 4.0

<div align="center">![revoultion](/extras/11.png)</div>

#### IOT Platform

 * All the IoT devices are connected to other IoT devices and application to transmit and receive information using protocols. There is a gap between the IoT device and IoT application. An IoT Platform fills the gap between the devices (sensors) and application (network).

 <div align="center">![revoultion](https://static.javatpoint.com/tutorial/iot/images/iot-platform.png)</div>

 1. **Amazon Web Services (AWS) IoT platform:** Amazon Web Service IoT platform offers a set of services that connect to several devices and maintain the security as well. This platform collects data from connected devices and performs real-time actions.

2. **Microsoft Azure IoT platform:** Microsoft Azure IoT platform offers strong security mechanism, scalability and easy integration with systems. It uses standard protocols that support bi-directional communication between connected devices and platform. Azure IoT platform has an Azure Stream Analytics that processes a large amount of information in real-time generated by sensors.

3. **Google Cloud Platform IoT:** Google Cloud Platform is a global cloud platform that provides a solution for IoT devices and applications. It handles a large amount of data using Cloud IoT Core by connecting various devices. It allows to apply BigQuery analysis or to apply Machine learning on this data.


4. IBM Watson IoT platform: The IBM Watson IoT platform enables the developer to deploy the application and building IoT solutions quickly. This platform provides the following services:

* Real-time data exchange
* Device management
* Secure Communication
* Data sensor and weather data services







